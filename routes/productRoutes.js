const express = require('express');
const router = express.Router();
const {
  authenticateUser,
  authorizePermissions,
} = require('../middleware/authentication');

const {
  createProduct,
  getAllProducts,
  getSingleProduct,
  updateProduct,
  deleteProduct,
  archiveProduct,
  activateProduct,
  uploadImage,
  getAllActiveProducts,
} = require('../controllers/productController');

const { getSingleProductReviews } = require('../controllers/reviewController');




router
  .route('/')
  .post([authenticateUser, authorizePermissions('admin')], createProduct)
  .get(getAllProducts);

  router.route('/activeProducts').get(getAllActiveProducts); 

router
  .route('/uploadImage')
  .post([authenticateUser, authorizePermissions('admin')], uploadImage)

router
  .route('/singleProduct/:id')
  .get(getSingleProduct)
  .patch([authenticateUser, authorizePermissions('admin')], updateProduct)
  .delete([authenticateUser, authorizePermissions('admin')], deleteProduct)

  

router.route('/reviews/:id').get(getSingleProductReviews)

router.route('/archiveProduct/:id').put([authenticateUser, authorizePermissions('admin')], archiveProduct)
router.route('/activateProduct/:id').put([authenticateUser, authorizePermissions('admin')], activateProduct)



module.exports = router;
